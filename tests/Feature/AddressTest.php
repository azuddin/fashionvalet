<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AddressTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testGetAddress()
    {
        $response = $this->get('/api/address');

        $response->assertStatus(200);
    }

    public function testCreateInvalidAddress()
    {
        $response = $this->withHeaders([
            'Accept' => 'application/json'
        ])->json('POST', '/api/address', [
            'address_1' => 'Business Office, Malcolm Long 92911 Proin Road Lake Charles Maine'
        ]);

        $response->assertStatus(422);
    }

    public function testCreateValidAddress()
    {
        $response = $this->withHeaders(['Accept' => 'application/json'])
        ->json('POST', '/api/address', [
            'address_1' => 'Business Office, Malcolm Long ',
            'address_2' => '92911 Proin Road Lake Charles ',
            'address_3' => 'Maine',
        ]);

        $response->assertStatus(200);
    }
}
