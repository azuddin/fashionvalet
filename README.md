## Installation:

```bash
composer install
php artisan key:generate
php artisan migrate
php artisan serve
```

## Available endpoint are: 
- GET /api/address
- POST /api/address

## Find POSTMAN collection at:
[fashionvalet.postman_collection.json](fashionvalet.postman_collection.json)

## Running test using this command:
```bash
php artisan test
```