<?php

namespace App\Http\Controllers;

use App\Address;
use App\User;
use Illuminate\Http\Request;

class AddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Address::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'address_1' => 'required|max:30',
            'address_2' => 'max:30',
            'address_3' => 'max:30',
        ]);

        $user =  User::first();
        if ($user==null) {
            $user =  User::create([
                'name' => 'Azuddin',
                'email' => 'ahmad@azuddin.com',
                'password' => bcrypt('123')
            ]);
        }

        $address = Address::create([
            'user_id' => $user->id,
            'address_1' => $validated['address_1'],
            'address_2' => $validated['address_2'],
            'address_3' => $validated['address_3']
        ]);

        return response(['data' => $address, 'message' => 'save successfully'], 200);
    }
}
